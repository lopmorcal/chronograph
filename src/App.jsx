
import './App.scss';
import { DigitalClock, Countdown, Stopwatch, Header } from './components';
import {Routes, Route} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route path='/clock' element={<DigitalClock/>}></Route>
        <Route path='/countdown' element={<Countdown/>}></Route>
        <Route path='/chrono' element={<Stopwatch/>}></Route>
      </Routes>
    </div>
  );
}

export default App;
