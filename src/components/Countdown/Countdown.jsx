import React, { useState, useEffect } from "react";
import './Countdown.scss'

const Countdown = () => {
  const [time, setTime] = useState("");
  const [date, setDate] = useState("");
  useEffect(() => {

    if (date) {
    let countDownDate = new Date(date).getTime();
    let x = setInterval(() => {
      let now = new Date().getTime();
      let distance = countDownDate - now;
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);

      setTime(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");

      if (distance < 0) {
        clearInterval(x);
        setTime("COUNTDOWN FINISHED");
      }
    }, 1000);
}
  }, [date]);

  return (
    <div className="countdown">
      <div className="countdown--zoneControl">
      <h2 className="countdown--title">Tiempo para la llegada a Marte</h2>
      <div className="countdown--info">{time}</div>
      <h2 className="countdown--title">Introduce la fecha de llegada</h2>
      <input className="countdown--input" onChange={(ev)=>{setDate(ev.target.value)}} type="date"/>
      </div>
    </div>
  );
};

export default Countdown;
