import React from 'react'
import {Link} from 'react-router-dom'
import './Header.scss'

const Header = () => {
  return (
    <div className='header'>
        <h1 className='header--title'>WELCOME TO ENTERPRISE</h1>
        <div className='navButtons'>
        <Link to="/clock"><button className='navButtons--button' >Reloj</button></Link>
        <Link to="/countdown"><button className='navButtons--button' >Cuenta atras</button></Link>
        <Link to="/chrono"><button className='navButtons--button' >Crono </button></Link>
        </div>
    </div>
  )
}

export default Header